'use strict';
//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js
//= ../node_modules/readmore-js/readmore.min.js"

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js
//= library/wow.js
//= library/slick.js


$(document).ready(function () {

    $(".wheretobuy .card").on("click", function () {
        $( this ).parent().find(".card").removeClass("active");
        $( this ).addClass("active");
    });

    $(".dos__item").on("click", function (e) {

        var parent = $(this).closest(".step__body");


        var container = $(parent).find(".step__body__second");


        $(container).find(".step__body__second__item img").attr("src", $(this).find("img").attr("src") );
        $(container).find(".step__body__second__header").html( $(this).attr("data-header") );
        $(container).find("p").html( $(this).attr("data-description") );
    });

    $('.article').readmore({
        speed: 1000,
        lessLink: "<span class='interesting__gov__item__more d-flex justify-content-start'><a href='#' class='products__link_more d-flex align-items-center'>Читать далее</a><div class='sldier_arrow_next'><svg class='card_item_arrow' xmlns='http://www.w3.org/2000/svg'  viewBox='0 0 18.098 10.316'><g id='arrow-up' transform='translate(-1035.793 -4349.188)''><g id='arrow-point-to-right' transform='translate(1053.891 4252.049) rotate(90)' opacity='0.303'><path id='Path_6' data-name='Path 6' d='M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z' transform='translate(0 0)'/></g></g></svg></div></span>",
        moreLink: "<span class='interesting__gov__item__more d-flex justify-content-start'><a href='#' class='products__link_more d-flex align-items-center'>Читать далее</a><div class='sldier_arrow_next'><svg class='card_item_arrow' xmlns='http://www.w3.org/2000/svg'  viewBox='0 0 18.098 10.316'><g id='arrow-up' transform='translate(-1035.793 -4349.188)''><g id='arrow-point-to-right' transform='translate(1053.891 4252.049) rotate(90)' opacity='0.303'><path id='Path_6' data-name='Path 6' d='M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z' transform='translate(0 0)'/></g></g></svg></div></span>",
        collapsedHeight: 150,
    });

    $(".btn__filter").on("click", function(e) {
        var paddingTop = $(".header").height();
        $(".filter__container").css('top', paddingTop);
        $(".filter__container").css('display', 'block');

        $(".header__top").css("box-shadow", "unset");
    });
    $(".filter__header__close").on('click', function (e) {
        $(".filter__container").css('display', 'none');

        $(".header__top").css("box-shadow", "0 0 10px rgba(0,0,0,.2)");
    });

    $(window).resize(function () {
        initPage();
    });

    function initPage() {
        if ( $(window).width() > 991 ) {
            $(".filter__container").css('display', 'block');
        } else {
            $(".filter__container").css('display', 'none');
        }
    }



    /* START Открытие меню */
    $(".navigation__content__link_wrap .arrow__click").on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(this).parent().find('.navigation__content__deep').addClass("active");
    });

    $(".navigation__content__link_back").on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(this).parent().removeClass("active");
        $(this).parent().find(".navigation__content__deep").removeClass("active");
    });

    $(".navigation__content__link_back_main").on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(".navigation__container").removeClass("active");
        $(".btn__menu").removeClass("active");
        $("#burger").removeClass("active");
        offScrollWindow();
    });

    function offScrollWindow () {
        $("body").toggleClass("body_scroll");
    }



    $(".btn__menu").on("click", function () {
        $(this).toggleClass("active");

        if ( $(this).hasClass("active") ) {
            $(".navigation__container").addClass("active");
            offScrollWindow();
        } else {
            $(".navigation__container").removeClass("active");
        }
    });

    $(".btn_header").on("click", function () {
        if( $(".navigation__container").hasClass("active") ) {
            $(".navigation__container").removeClass("active");
            $(".btn__menu").removeClass("active");
            $("#burger").removeClass("active");
        }
    });
    /* END откртие меню*/


	$("input[type=radio]").click(function(){
		//console.log( $(this).val() );
	});




	/* START preloader*/
	(function () {
		if ( Date.now() - preloader.timerStart >= preloader.duration  ) {
			$("#preloader-cube").animate({"opacity" : "0"}, preloader.duration / 2, function () {
				$("#preloader-cube").css("display", "none");
			});
		} else {
			setTimeout(function () {
				$("#preloader-cube").animate({"opacity" : "0"}, preloader.duration / 2, function () {
					$("#preloader-cube").css("display", "none");
				});
			}, preloader.duration / 1.7)
		}
	})();
	/* END preloader */


	if ( $(window).scrollTop() > 0 ) {
        //$("header").addClass("bg-dark");
    }




	$(window).on("scroll", function () {
		var heightTop = $(".first-header").height();
		heightTop += parseInt($(".first-header").css("padding-top"));
		heightTop += parseInt($(".first-header").css("padding-bottom"));
		var paddingTop = $(".header").height();


		if ( $(window).scrollTop() > heightTop ) {
            $(".header").css("position", "fixed");
            $(".schedule .email_header").css("display", "flex");
            $(".schedule__date").css("display", "none");

			if ( $(window).width() > 991 ) {
                $(".content").css("margin-top", paddingTop );

               console.log( $(window).width());
               console.log(heightTop);

                
            } else {
                 $(".header__top__mobile ").css("margin-top", paddingTop);
            }
		}  else {
			$(".header").css("position", "unset");

			$(".header__top__mobile").css("margin-top", "0");
            $(".content").css("margin-top", 0);

			$(".schedule .email_header").css("display", "none");
			$(".schedule__date").css("display", "flex");
		}

		/* START кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$("#btn-to-top").addClass("btn-to-top_active");
		} else {
			$("#btn-to-top").removeClass("btn-to-top_active");
		}

	});
	$('#btn-to-top, .to_top_click').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
	/* END кнопка вверх */










	$('#modalCalc').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) ;
		var recipient = button.data('whatever') ;
		//var modal = $(this);
		console.log(recipient);
		//modal.find('.modal-body .calc__val').val(recipient);
	})
	/* END calc event */
    $('.youCheckedItems').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        adaptiveHeight: true,
        lazyLoad: 'ondemand',
        slidesToScroll: 2,
        nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
        prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 981,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });





	$('#products__slider').slick({
		dots: true,
		infinite: false,
		speed: 300,
		slidesToShow: 4,
		adaptiveHeight: true,
        lazyLoad: 'ondemand',
		slidesToScroll: 2,
		nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
		prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
		responsive: [
			{
				 breakpoint: 1160,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 981,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		]
	});
    $('#products__slider_mobile').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        lazyLoad: 'ondemand',
        adaptiveHeight: true,
        slidesToScroll: 1,
        nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
        prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
    });

    $('.qproducts__slider_mobile').slick({
        dots: false,
        infinite: false,
        speed: 300,
        lazyLoad: 'ondemand',
        slidesToShow: 1,
        adaptiveHeight: true,
        slidesToScroll: 1,
        nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
        prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
    });


    $('.products__sliderq').slick({
        dots: false,
        infinite: false,
        lazyLoad: 'ondemand',
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        slidesToScroll: 1,
        nextArrow: "",
        prevArrow: "",

    });


    $('.choose__two_slider').slick({
        dots: true,
        infinite: false,
        lazyLoad: 'ondemand',
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        slidesToScroll: 1,
        nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
        prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
        
    });

    $(".choose__two__nav .sldier_arrow_next.next").click(function () {
        $('.products__sliderq').slick('slickNext');

        var index = $('.products__sliderq').slick("slickCurrentSlide");

        var items = $('.choose__two__header');

        $(".choose__two__header").removeClass("active");
        $(items[index]).addClass("active");
    });
    $(".choose__two__nav .sldier_arrow_next.prev").click(function () {
        $('.products__sliderq').slick('slickPrev');

        var index = $('.products__sliderq').slick("slickCurrentSlide");

        var items = $('.choose__two__header');

        $(".choose__two__header").removeClass("active");
        $(items[index]).addClass("active");
    });


    $(".choose__two__header:nth-child(1)").click(function () {
        $(".choose__two__header").removeClass("active");
        $(this).addClass("active");
        $('.products__sliderq').slick('slickGoTo', 0);
    });
    $(".choose__two__header:nth-child(2)").click(function () {
        $(".choose__two__header").removeClass("active");
        $(this).addClass("active");
        $('.products__sliderq').slick('slickGoTo', 1);
    });
    $(".choose__two__header:nth-child(3)").click(function () {
        $(".choose__two__header").removeClass("active");
        $(this).addClass("active");
        $('.products__sliderq').slick('slickGoTo', 2);
    });
    $(".choose__two__header:nth-child(4)").click(function () {
        $(".choose__two__header").removeClass("active");
        $(this).addClass("active");
        $('.products__sliderq').slick('slickGoTo', 3);
    });

    $('#video__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '#video__slider_dots',
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    asNavFor: false,
                    dots: true,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    $('#video__slider_dots').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '#video__slider',
        dots: true,
        centerMode: true,
        focusOnSelect: true,
        nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
        prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',

    });


	$(".products__form").submit(function (e) {
        e.preventDefault();
        e.stopPropagation();

        console.log("ok");
    });


    (function() {
        var x, i, j, selElmnt, a, b, c, wrapperHeader ;
        /* Look for any elements with the class "custom__select": */
        x = document.getElementsByClassName("custom__select");
        for (i = 0; i < x.length; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0];
            /* For each element, create a new DIV that will act as the selected item: */
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");

            wrapperHeader = document.createElement("DIV");
            wrapperHeader.setAttribute("class", "text__underline");
            wrapperHeader.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            a.appendChild (wrapperHeader);

            x[i].appendChild(a);

            /* For each element, create a new DIV that will contain the option list: */
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            for (j = 1; j < selElmnt.length; j++) {
                /* For each option in the original select element,
                create a new DIV that will act as an option item: */
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.addEventListener("click", function(e) {
                    /* When an item is clicked, update the original select box,
                    and the selected item: */
                    var y, i, k, s, h;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    h = this.parentNode.previousSibling;

                    for (i = 0; i < s.length; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            console.log(h);
                            //h.innerHTML = this.innerHTML;
                            $(this).parent(".select-items").parent(".custom__select").find(".text__underline").html(this.innerHTML);
                            console.log(this);
                            console.log("Name select: " + this.innerHTML);
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            for (k = 0; k < y.length; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();

                    if ( !( $(this).parent(".select-items").parent(".custom__select").find("select").val() == 0 ) ) {
                        $(this).parent(".select-items").parent(".custom__select").find(".select-selected").addClass("select-selected__active");
                    }
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);
            a.addEventListener("click", function(e) {
                /* When the select box is clicked, close any other select boxes,
                and open/close the current select box: */
                e.stopPropagation();
                closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");



            });
        }



        function closeAllSelect(elmnt) {
            /* A function that will close all select boxes in the document,
            except the current select box: */
            var x, y, i, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            for (i = 0; i < y.length; i++) {
                if (elmnt == y[i]) {
                    arrNo.push(i)
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < x.length; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }




        }

        /* If the user clicks anywhere outside the select box,
        then close all select boxes: */
        document.addEventListener("click", closeAllSelect);
    })();



    if ( $("div").is(".form__numbers") ) {
        $(".number__minus").on("click", function (e) {
            e.preventDefault();
            inputNumber("minus", this);
        });
        $(".number__plus").on("click", function (e) {
            e.preventDefault();
            inputNumber("plus", this);
        });
    }

    function inputNumber(operant, context) {
        var obj = $(context).parent().find("input[type='number']");
        if ( operant == "plus" ) {
            $(obj).val( parseInt($(obj).val()) + 1 );
        } else if ( operant == "minus" ) {
            if ( $(obj).attr("min") < $(obj).val() )
                $(obj).val( parseInt($(obj).val()) - 1 );
        }
        $(obj).parent().find(".form__numbers_val").html( $(obj).val() );
    }








    // ховер на товар
    getHeightProducts();
    function getHeightProducts() {
        var items = $(".products .products__item").parent();
        $(".products .products__item").parent().css("overflow", "");
        console.log(items);
        var maxHeight = 0;

        var length = items.length;
        for ( var i = 0; i < length; i++ ) {
            if ( $(items[i]).height() > maxHeight )
                maxHeight = $(items[i]).height();
        }
        console.log(maxHeight + 183);
        //$(".products .products__item").parent().height(maxHeight + 140);
    }







    // читать далее
    $(".btn__text_more").on("click", function (e) {
        e.stopPropagation();
        e.preventDefault();

        var heigheContent = $(this).parent().parent().find(".hid__content__wrap").innerHeight();

       if ( $(this).hasClass("active") ) {
         $(this).parent().parent().find(".hid__content").animate({'height':heigheContent}, 500);
       } else {
        $(this).parent().parent().find(".hid__content").animate({'height':"240"}, 500);
       }


        $(this).toggleClass('active');
    });


    // открытие меню в советах
    $(".help__link_wrap").on('click', function (e) {
        if ( $(window).width() < 991 ) {
            e.preventDefault();
            e.stopPropagation();

            $(this).find(".help__link__content").addClass("active");
            $(this).parent().parent().parent().find(".asd .sldier_arrow_next").css("display", 'none');


        }
    });

    $(".help__link__content__back").on("click", function () {
        $(this).parent().parent().removeClass("active");
        $(this).parent().parent().parent().find(".asd .sldier_arrow_next").css("display", 'block');
    });


    // hover эффект для товаров

    $(".products__item ").hover(function (e) {
        console.log("fiddddd");
        //$(this).css("position", "fixed");

    });


    $(".order_min").click(function (e) {
        
    });





    $(".modalCalculate").on('click', function (e) {
        e.stopPropagation();
        e.preventDefault();
    });

});


